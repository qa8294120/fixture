

describe('Sign In', () => {
  beforeEach(() => {
    cy.visit('https://next-realworld.vercel.app/user/login');
  });

  it('should sign in successfully with valid credentials', () => {
    cy.fixture('signin').then((data) => {
      const user = data.validUser;
      cy.get(':nth-child(1) > .form-control').clear().type(user.email);
      cy.get(':nth-child(2) > .form-control').clear().type(user.password);
      cy.get('.btn').click();
      cy.get('.navbar').should('be.visible')
    });
  });

  it('should show error for invalid password', () => {
    cy.fixture('signin').then((data) => {
      const user = data.invalidPassword;
      cy.get(':nth-child(1) > .form-control').clear().type(user.email);
      cy.get(':nth-child(2) > .form-control').clear().type(user.password);
      cy.get('.btn').click();
      cy.get('.error-messages > li').should('be.visible')
    });
  });

  it('should show error for invalid email', () => {
    cy.fixture('signin').then((data) => {
      const user = data.invalidEmail;
      cy.get(':nth-child(1) > .form-control').clear().type(user.email);
      cy.get(':nth-child(2) > .form-control').clear().type(user.password);
      cy.get('.btn').click();
      cy.get('.error-messages > li').should('be.visible')
    });
  });

  it('should show error for empty email', () => {
    cy.fixture('signin').then((data) => {
      const user = data.emptyEmail;
      //cy.get(':nth-child(1) > .form-control').clear().type(user.email);
      cy.get(':nth-child(2) > .form-control').clear().type(user.password);
      cy.get('.btn').click();
      cy.get('.error-messages > li').should('be.visible')
    });
  });

  it('should show error for empty password', () => {
    cy.fixture('signin').then((data) => {
      const user = data.emptyPassword;
      cy.get(':nth-child(1) > .form-control').clear().type(user.email);
      //cy.get(':nth-child(2) > .form-control').clear().type(user.password);
      cy.get('.btn').click();
      cy.get('.error-messages > li').should('be.visible')
    });
  });

  it('should show error for using number in place of email', () => {
    cy.fixture('signin').then((data) => {
      const user = data.usingNumber;
      cy.get(':nth-child(1) > .form-control').clear().type(user.email);
      cy.get(':nth-child(2) > .form-control').clear().type(user.password);
      cy.get('.btn').click();
      cy.get('html').should('be.visible')
    });
  });

  it('should show error for email sessitivity case', () => {
    cy.fixture('signin').then((data) => {
      const user = data.emailSensitive;
      cy.get(':nth-child(1) > .form-control').clear().type(user.email);
      cy.get(':nth-child(2) > .form-control').clear().type(user.password);
      cy.get('.btn').click();
      cy.get('.error-messages > li').should('be.visible')
    });
  });

  it('should show error for empty credentials', () => {
    cy.fixture('signin').then((data) => {
      const user = data.allEmpty;
      cy.get('.btn').click();
    
    });
  });

  it('should show error for password case sensitivity case', () => {
    cy.fixture('signin').then((data) => {
      const user = data.passwordSensitive;
      cy.get(':nth-child(1) > .form-control').clear().type(user.email);
      cy.get(':nth-child(2) > .form-control').clear().type(user.password);
      cy.get('.btn').click();
      cy.get('.error-messages > li').should('be.visible')
    });
  });

  

});
